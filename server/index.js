var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var User = require('./User.js');
var user = new User();
var Channel = require('./Channel.js');
var channel = new Channel(/*User.users*/);
app.get('/', function(req, res){});

io.on('connection', (socket) => {
  var room = '';
  console.log('connected user');
  socket.on('save_user', (nickname) => {
    let err = user.setUser(nickname, socket.id);
    socket.emit('error_save_user', err);
  });


  socket.on('create_channel', (channelName) => {
    var addedUser = user.findById(socket.id)
    var error = channel.addChannel(channelName, addedUser);
    if (error.ok)
    {
      socket.leave(room, () => {
        channel.deleteUserInChannel(room, addedUser)
        error.message = "Vous avez quitter le channel " + room + "/n" + error.message;
      })
      socket.join(channelName, () => {
        room = channelName;
        socket.emit('error_find_user_channel', channel.findUserInChannel(channelName));
      });
    }
    error.creater = addedUser;
    socket.emit('error_create_channel', error)
  });

  socket.on('find_user_channel', (channelName) => {
    socket.emit('error_find_user_channel', channel.findUserInChannel(channelName));
  });

  socket.on('list_user', () => {
    socket.emit('display_list_user', User.users);
  });

  socket.on('update_user', (newNickname) => {
    socket.emit('error_update_user', user.updateUser(newNickname, socket.id))
  })

  socket.on('list_channel', () => {
    socket.emit('display_list_channel', Channel.channels)
  })

  socket.on('send_message', function(msg){
    data = {m:msg, a:user.findById(socket.id)}
    io.in(room).emit('new_message', data);
  });

  io.on('disconnect', () => {
    console.log('user disconnected');
  });
});

http.listen(4242, 'localhost', function(){
  console.log('listening on *:4242');
});
