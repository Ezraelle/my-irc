class User{
  static users = [];

  nicknameIsUnique(nickname, id) {
    if (User.users.indexOf(nickname) === -1)
      return true;
    return false;
  }

  setUser(nickname, id) {
    if (!this.checkNickname(nickname))
      return ({'ok': false, 'message': 'Veillez choisir un nickname entre 2 et 25 carractères'})
    if (!this.findByNickname(nickname))
    {
      User.users.push({id: id, nickname: nickname});
      return ({'ok': true, 'message': 'Vous êtes bien connectez en tant que ' + nickname})
    }
    return ({'ok': false, 'message': 'le nickame ' + nickname + ' déjà utilisé'})
  }

  updateUser(nickname, id) {
    var user = this.findById(id);
    if (!this.checkNickname(nickname))
      return ({'ok': false, 'message': 'Veillez choisir un nickname entre 2 et 25 carractères'})
    if (user && !this.findByNickname(nickname))
    {
      this.deleteUser(user)
      User.users.push({id: id, nickname: nickname});
      return ({'ok': true, 'message': 'Vous êtes bien connectez en tant que ' + nickname})
    }
    return ({'ok': false, 'message': 'le nickame ' + nickname + ' déjà utilisé'})
  }

  deleteUser(user) {
    var key = User.users.indexOf(user);
    if (key !== -1)
      User.users.splice(key, 1);
  }

  findById(id) {
    var userGetted;
    User.users.map(user => {
    if (user.id === id)
      userGetted = user;
    })
    return userGetted;
  }

  findByNickname(nickname) {
    var userGetted = false;
    User.users.map(user => {
    if (user.nickname === nickname)
      userGetted = user;
    })
    return userGetted;
  }

  checkNickname(nickname) {
    if (nickname.length < 2 || nickname.length > 25)
      return false;
    return true;
  }

  // updateNickname(newNickname, id) {
  //   if (nicknameIsUnique) {
  //     this.setUsers(newNickname, id);
  //   }
  // }
}


module.exports = User
