class Channel {

  static channels = [];

  addChannel(channel, addedUser) {
    if (!this.channelExist(channel.name))
    {
      Channel.channels.push({channelName: channel, channelUser:[addedUser], channelCreatedBy: addedUser})
      return ({"ok": true, "message": "Le channel : " + channel + " à été créé" });
    }
    this.addUserInChannel(channel, addedUser);
    return ({"ok": true, "message" : "Vous avez rejoint le " + channel});
  }

  addUserInChannel(channel, user)
  {
    Channel.channels.map((chan) => {
      if (chan.channelName.name == channel.name) {
        chan.channelUser.push(user);
      }
    })
  }

  deleteUserInChannel(channelName, user) {
    var channel = this.channelExist(channelName);
    if (channel)
    {
      var key = channel.channelUser.indexOf(user);
      channel.channelUser.splice(key, 1);
    }
  }

  findUserInChannel(channelName) {
    var channel = this.channelExist(channelName.name);
    if (channel)
    {
      return ({'ok': true, 'message': channel.channelUser});
    }
    else
    {
      return ({'ok': false, 'message': "Vous êtes le premier à joindre " + channelName.name});
    }
  }

  channelExist(channelName) {
    var ret = false
    Channel.channels.map((channel) => {
      if (channel.channelName.name == channelName) {
        ret = channel;
      }
    });
    return ret;
  }
}

module.exports = Channel;
