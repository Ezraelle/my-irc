import React from 'react';
import Chat from './components/Chat';
import InputNickname from './components/InputNickname';
import Menu from './components/Menu';
import List from './components/List';
import './css/app.css';

import socketIOClient from 'socket.io-client';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.handleNicknameSet = this.handleNicknameSet.bind(this);
    this.setUserOrdisplayChat = this.setUserOrdisplayChat.bind(this);
    this.socket = socketIOClient('http://localhost:4242');
    this.state = {
      users: [],
      channels: [],
      usersChannel: [],
      displayInput: false,
      fonctionUser: this.handleNicknameSet,
      error: '',
      channel: {name: 'Géneral', creater: ''}
    }
    // this.getUserChannel(this.state.channel);
    this.socket.on('display_list_user', (users) => {
      this.setState({users: users})
    });
    this.socket.on('display_list_channel', (channels) => {
      this.setState({channels: channels})
    });
    this.socket.on('error_find_user_channel', (channelUsers) => {
      if ('on : ',channelUsers.ok)
        this.setState({usersChannel: channelUsers.message})
      else
        this.setState({error: channelUsers.message});
    });
  }

  handleNicknameSet(nickname) {
    this.socket.emit('save_user', nickname);
    this.socket.on('error_save_user', (error) => {
      if (error.ok)
      {
        this.socket.emit('create_channel', this.state.channel);
        this.setState({displayInput: true});
      }
      else
        this.setState({displayInput: false});
      this.setState({error: error.message})
    });
  }

  setUserOrdisplayChat() {
    if (this.state.displayInput)
    {
      return (
        <Chat
        socket={this.socket}
        error={this.state.error}
        />
      );
    }
    return (
      <InputNickname
        socket={this.socket}
        fonctionUser={this.state.fonctionUser}
        error={this.state.error}
      />
    )
  }

  handleClickListUser = () => {
    this.socket.emit('list_user', '');
  }

  handleClickListChannel = () => {
    this.socket.emit('list_channel', '');
  }

  handleClickCreateChannel = () => {
    this.setState({
      fonctionUser: this.handleCreateChannel,
      displayInput: false
    });
  }

  handleClickNickname = () => {
    this.setState({
      fonctionUser: this.handleUpdateNickname,
      displayInput: false,
    })
  }

  handleUpdateNickname = (newNickname) => {
    this.socket.emit('update_user', newNickname);
    this.socket.on('error_update_user', (error) => {
      if (error.ok)
        this.setState({displayInput: true});
      else
        this.setState({displayInput: false});
      this.setState({error: error.message})
    });
  }

  handleCreateChannel = (channel) => {
    this.socket.emit('create_channel', channel);
    this.socket.on('error_create_channel', (error) => {
      if (error.ok)
      {
        console.log('PUTE : ', error);
        this.setState({displayInput: true, channel: {name:channel, creater: error.creater}});
        this.getUserChannel(channel);
      }
      else
        this.setState({displayInput: false});
    this.setState({error: error.message})
    })
  }

  getUserChannel = (channel) => {
    this.socket.emit('find_user_channel', channel);
  }

  render() {
    console.log(this.state.channel);
    return (
      <div className="main">
        <div id="inside">
        <aside>
          <h1> {this.state.channel.name} </h1>
          <h4> by {this.state.channel.creater.nickname} </h4>
          <List
          func= 'usersChannel'
          data= {this.state.usersChannel}
          />
        </aside>
        <section>

          <Menu
          onClickListUser={this.handleClickListUser}
          onClickListChannel={this.handleClickListChannel}
          onClickUpdateNickname={this.handleClickNickname}
          onClickCreateChannel={this.handleClickCreateChannel}
          />
          {this.setUserOrdisplayChat()}
        </section>
        <aside id="list_user_chan">
        <List
        func= 'user'
        data= {this.state.users}
        />
        <List
        func= 'channel'
        data= {this.state.channels}
        />
        </aside>
        </div>
      </div>
    )

  }
}
export default App;
