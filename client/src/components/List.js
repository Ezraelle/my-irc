import React from 'react';

class List extends React.Component {
  constructor(props) {
    super(props);
    this.mode = {
      user: this.listUser,
      channel: this.listChannel,
      usersChannel: this.listUserChannel,
      channelName: this.channelName
    }
  }

  listUser = () => {
    return (
      <ul id="list_user">
      {this.props.data.map((user) => {
        return <li key={user.id}> {user.nickname}</li>
      })}
      </ul>
    );
  }

  listChannel = () => {
    return (
      <ul id="list_channel">
      {this.props.data.map((channel) => {
        return <li key={channel.channelName}> {channel.channelName}</li>
      })}
      </ul>
    );
  }

  listUserChannel = () => {
    console.log('data', this.props.data);
    return (
      <ul id="list_user_channel">
      {this.props.data.map((channelUser, index) => {
        return <li key={index}> {channelUser.nickname}</li>
      })}
      </ul>
    );
  }

  render() {
    var func = this.mode[this.props.func]
    return (
        <div id="list">
          {func()}
        </div>
    );
  }
}


export default List;
