import React from "react";

class InputNickname extends React.Component {
  constructor (props) {
    super (props);
    this.socket = props.socket;
  }

  handleSubmit(e) {
    e.preventDefault();
    // console.log(this.props);
    var data = document.getElementById('data').value;
    this.props.fonctionUser(data);
  }

  render () {
    return (
      <div>
      <form id="form_go" onSubmit={(e) => this.handleSubmit(e)}>
      <p> {this.props.error} </p>
      <input id="data" type="text" />
      <input type="submit" id="go" value="GO" />
      </form>
      </div>
    )
  }
}

export default InputNickname;
