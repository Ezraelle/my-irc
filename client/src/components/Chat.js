import React from "react";
// import socketIOClient from 'socket.io-client';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.socket = props.socket
    this.state= {
      messages: [],
    }
    this.socket.on('new_message', (msg) => {
      var messages = this.state.messages;
      messages.push(msg.a.nickname + " : " + msg.m)
      this.setState({messages: messages});
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    this.socket.emit('send_message', event.target.children[0].value);
  }

  render()
  {
    return (
      <div id="out_chat">
      <div id="chat">
      <div id="in_chat">
      <p id="p_messages"> {this.props.error} </p>
        <ul id="messages">
        {this.state.messages.map((msg) => {
          return (
            <li key={Math.random().toString(36).substr(2)}> {msg} </li>
          );
        })}
        </ul>
      </div>
      </div>
      <div>
      <form id="send_message" onSubmit={(event) => this.handleSubmit(event)} action="">
      <input id="m" autoComplete="off" /><button id="send" >Send</button>
      </form>
      </div>
      </div>

    )
  }
}

export default Chat;
