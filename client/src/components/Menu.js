import React from "react";

class Menu extends React.Component {
  render() {
    return (
      <nav id="nav">
        <button className="btn_nav"
          onClick={this.props.onClickListUser}
        > Liste utilisateurs </button>
        <button className="btn_nav"
          onClick={this.props.onClickListChannel}
        > Liste channels </button>
        <button className="btn_nav"
          onClick={this.props.onClickUpdateNickname}
        > Changer pseudo </button>
        <button className="btn_nav"
          onClick={this.props.onClickCreateChannel}
        > Créer/Joindre un channel </button>
      </nav>
    )
  }
}

export default Menu;
